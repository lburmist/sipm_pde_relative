# sipm_pde_relative

Project to analyse SiPM reverse and forward IV curves. As well as for relative PDE analysis. 
Project containes:
-   IV.py                       -> calss to display and basic IV analysi;
-   reverse_iv.py               -> dougter class of IV.py to calcultate relative PDE
-   analysis_script.ipynd       -> jupiter notebook with exapels how to analyse relative PDE data and use IV.py and reverse_iv.py
-   PD_S1337-1010BQ_SN01.out    -> QE for calibrated photodiode
-   results                     -> folder with some saved results
-   images                      -> folder with some images used to discribe analysis procedure
-   data_example                -> filder with experimental data

For more detailes, please have a look: [Characterisation of a large area silicon photomultiplier](https://arxiv.org/pdf/1810.02275.pdf)
